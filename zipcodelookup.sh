#!/bin/bash

#basic zip code look up script
# First set the url we will be using
url="http://www.city-data.com/zips"

# Will ask that zipcode the user will want to look up and recive input
echo "What zipcode would you like to look up?"
read zipcode

# echo the response
echo ""
echo "The zip code of $zipcode is located in"
# first we need to curl from the website the title. Then using awk we will only print what is inisde  the () which is the state and city 
curl -s -dump "$url/$zipcode.html" | grep -i '<title>' | awk -F "[()]" '{ for (i=2; i<NF; i+=2) print $i }'
echo ""

# Also echo the website so the user can read more into it. 
echo "For more infomation go to this site $url/$zipcode.html"
echo ""
exit 0 
	
