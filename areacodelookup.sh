#!/bin/bash

#basic zip code look up script
# First set the url we will be using
url="https://www.bennetyee.org/ucsd-pages/area.html"

# Will ask that zipcode the user will want to look up and recive input
echo "What Area Code would you like to look up?"
read areacode

# echo the response
echo ""
echo "The area code of $areacode is located in"
# This part got me a little bit. Since I did not want to copy from the book I had troubles getting awk working. But this is what I got. 
#First lets get the webpage source code. Then we will finter with grep by the name. We will then user awk to take seperate the line. I did it a second time becouse I was not sure how to compine the 2
# I found that their was a leading ( at the end of the string so i took it out with sed
curl -s -dump  "$url" | grep -i " name=\"$areacode\"" | awk -v FS='   ' '{print $2}' | awk -v FS='see' '{print $1}' | sed 's/(//'

echo ""

# Also echo the website so the user can read more into it. 
echo "For more infomation go to this site $url"
echo ""
exit 0 
